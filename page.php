<?php
/**
 * Page
 */

// * Timber Context
$context = Timber::get_context();

// * Post
$context['post'] = new \Timber\Post();

// * Load view
Timber::render('views/page.twig', $context);
