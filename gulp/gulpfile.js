// Locals
var argv            = require('minimist')(process.argv.slice(2));

// Globals
global.browserSync  = require('browser-sync').create();
global.del          = require('del');
global.lazypipe     = require('lazypipe');
global.gulp         = require('gulp');
global.gulpif       = require('gulp-if');
global.merge        = require('merge-stream');
global.plumber      = require('gulp-plumber');
global.runSequence  = require('run-sequence');

// CLI options
global.enabled = {
  // Disable source maps when `--prod`
  maps: !argv.prod,
  // Fail styles task on error when `--prod`
  failStyleTask: argv.prod,
  // Fail due to JSHint warnings only when `--prod`
  failESHint: argv.prod,
  // Strip debug statments from javascript when `--prod`
  stripJSDebug: argv.prod,
  // Minify CSS and JS when `--prod`
  minify: argv.prod,
  // Generate POT file when '--prod'
  generatePOT: argv.prod,
};

global.config = require('./config.json'),
global.deps   = config.dependencies,
global.paths  = config.paths;

require('require-dir')('./gulp-tasks');

gulp.task('default', ['clean'], function() {
  gulp.start('build');
});
