<?php

// * Timber Context
$context = Timber::get_context();

// * Load view
Timber::render( 'views/footer.twig', $context );
