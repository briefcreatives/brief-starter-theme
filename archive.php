<?php

/**
 * Archive Page
 */

// * Timber Context
$context = Timber::get_context();

// * Posts
$context['posts'] = new \Timber\PostQuery();

$post_type = get_post_type();
// * Load view
Timber::render( [ 'views/archive-' . $post_type . '.twig', 'views/archive.twig' ], $context );
