# [Brief Starter Theme](https://brief.pt)

This theme is a fork of [Sage](https://roots.io/sage/), a WordPress starter theme based on HTML5 Boilerplate and gulp.

It comes with [Timber](https://www.upstatement.com/timber/), a library for theme building with [Twig](https://twig.symfony.com/).

**Docs:**

* [Sage Docs](https://roots.io/sage/docs/)
* [Timber Docs](https://timber.github.io/docs/getting-started/setup/)
* [Twig Docs](https://twig.symfony.com/doc/1.x/)

---
## Requirements

| Prerequisite          | How to check  | How to install
| ---------------       | ------------  | ------------- |
| PHP >= 5.4.x          | `php -v`      | [php.net](http://php.net/manual/en/install.php) |
| Composer >= 1.5.x     | `composer -v` | [getcomposer.org](https://getcomposer.org/) |
| Node.js >= 4.5        | `node -v`     | [nodejs.org](http://nodejs.org/) |
| gulp >= 3.8.10        | `gulp -v`     | `npm install -g gulp` |

## Features

Development:

* [gulp](http://gulpjs.com/) build script that compiles SCSS, checks for JavaScript errors, optimizes images, and concatenates and minifies files
* [BrowserSync](http://www.browsersync.io/) for keeping multiple browsers and devices synchronized while testing, along with injecting updated CSS and JS into your browser while you're developing
* `gulp/config.json` to configure the build system

Backend:

* [Theme Wrapper](https://roots.io/sage/docs/theme-wrapper/)
* [Timber](https://www.upstatement.com/timber/)
* [Twig](https://twig.symfony.com/)
* [Upstatement/routes](https://github.com/Upstatement/routes)

Front-end:
 
* [Uikit](https://getuikit.com/)
* [Swiper](http://idangero.us/swiper/)

---

## Theme installation

1. Place project within your WordPress themes directory.
````shell
@ example.com/wp-content/themes/
$ git clone git@bitbucket.org:briefcreatives/brief-wordpress-starter-theme.git brief-theme
````

2. Install composer dependencies
````shell
@ example.com/wp-content/themes/brief-theme
$ composer install
````

3. Install npm dependencies
````shell
@ example.com/wp-content/themes/brief-theme
$ npm install
````

3. Compile Assets
````shell
@ example.com/wp-content/themes/brief-theme/gulp
$ gulp build
````
---

## Theme setup

Edit `lib/setup.php` to enable or disable theme features, setup navigation menus, post thumbnail sizes, post formats, and sidebars.

---

## Theme development

This theme uses [gulp](http://gulpjs.com/) as its build system and [npm](https://www.npmjs.com/) to manage front-end packages.

### Available gulp commands

* `gulp` — Compile and optimize the files in your assets directory
* `gulp watch` — Compile assets when file changes are made
* `gulp --prod` — Compile assets for production (no source maps).

### Using BrowserSync

To use BrowserSync during `gulp watch` you need to update `devUrl` at the bottom of `gulp/config.json` to reflect your local development hostname.

For example, if your local development URL is `http://project-name.dev` you would update the file to read:
```json
...
  "config": {
    "devUrl": "http://project-name.dev"
  }
...
```
If your local development URL looks like `http://localhost:8888/project-name/` you would update the file to read:
```json
...
  "config": {
    "devUrl": "http://localhost:8888/project-name/"
  }
...
```

---

## Theme Updates

This theme is ready for self-updates via the Wordpress update feature.

### Theme Updater (client):

For self-updates, it is necessary to import the Theme Update Checker file in `/lib/theme-update-checker.php` into the WP functions file. 


````php
...
// Theme Update Checker
if ( 'production' === WP_ENV ) {
  $includes[] = 'lib/theme-update-checker.php';
}
...
````
Optional: Import theme updater only on production enviroment.

After the file is imported, it is necessary to connect the Theme Update Checker with the server. 

Theme Slug - The slug for the theme. Should be the name present i the Theme Name in the `styles.css` file.

Update URL - URL for the update server


````php
...
// Run update checker on production site
if ( 'production' === WP_ENV ) {
  /**
   * Connect to WP Update Server @brief.pt running 
   * https://github.com/YahnisElsts/wp-update-server
   */
  $theme_slug = 'brief-theme'; //Theme slug. Usually the same as the name of its directory.
  $theme_update_checker = new ThemeUpdateChecker (
    $theme_slug, //Theme slug. Usually the same as the name of its directory.
    'https://updates.brief.pt/wp-update-server/?action=get_metadata&slug=' . $theme_slug //Metadata URL.
  );
}
...
````
Optional: Start theme updater only on production enviroment.

### Update Server:

To do this there should always be a remote server where the theme is kept, and from where Wordpress will fetch the latest announced version.

In the updates.brief.pt dropplet [WP Update Server](https://github.com/YahnisElsts/wp-update-server) is installed and ready to receive theme and plugin zips to generate the necessary metadata for Wordpress.

To send the project to the remote server, it its necessary to build the assets for production, zip it, and send it with `scp`.

````shell
@ example.com/wp-content/themes/brief-theme/gulp
$ gulp --prod 
$ gulp zip
$ scp ../../brief-theme.zip root@updates.brief.pt:/var/www/updates/wp-update-server/packages/
````
Note: `gulp zip` will zip the whole theme, except a few files, and place it in the themes folder. For further configuration visit `gulp/config.json`.

After sending the zipped project to the update server, the update URL (https://updates.brief.pt/wp-update-server/?action=get_metadata&slug=brief-theme) should return a json similar to this

````json
{
    "name": "Brief Theme",
    "version": "1.0.0",
    "homepage": "https:\/\/brief.pt",
    "author": "Brief Creatives",
    "author_homepage": "https:\/\/brief.pt",
    "details_url": "https:\/\/brief.pt",
    "last_updated": "2018-03-20 11:46:07",
    "slug": "brief-theme",
    "download_url": "https:\/\/updates.brief.pt\/wp-update-server\/?action=download&slug=brief-theme",
    "request_time_elapsed": "0.004"
}
````
After this, you can visit the Wordpress dashboard and the update option should appear.
