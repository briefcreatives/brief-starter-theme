<?php

// Define enviroment as production if WP_ENV is not set
if ( ! defined( 'WP_ENV' ) || ( 'development' !== WP_ENV && 'staging' !== WP_ENV ) ) {
	define( 'WP_ENV', 'production' );
}

// Initialize Timber and require composer depedencies before everything
require_once __DIR__ . '/vendor/autoload.php';
new Timber\Timber();

/**
 * Includes
 *
 * The $includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$includes = [];

// * Scripts and stylesheets
$includes[] = 'lib/assets.php';
// * Custom functions
$includes[] = 'lib/extras.php';
// * Theme setup
$includes[] = 'lib/setup.php';
// * Theme wrapper class
$includes[] = 'lib/wrapper.php';
// * Variables
$includes[] = 'lib/variables.php';
// * Post types registrator
$includes[] = 'lib/custom-post-types/cpt-registrator.php';
// * Helpers
$includes[] = 'lib/helpers/debug.php';
// * Hooks
$includes[] = 'lib/hooks/admin.php';
$includes[] = 'lib/hooks/uploads.php';

foreach ( $includes as $file ) {
	$filepath = locate_template( $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating %s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}
unset( $file, $filepath );
