<?php

/**
 * Single
 */


// * Timber Context
$context = Timber::get_context();

// * Post
$context['post'] = new \Timber\Post();

$post_type = get_post_type();

// * Load view
Timber::render( [ 'views/single-' . $post_type . '.twig', 'views/single.twig' ], $context );
