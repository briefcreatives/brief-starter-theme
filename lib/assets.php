<?php

namespace Brief\Assets;

/**
 * Get paths for assets
 *
 * @param string $file_path File path.
 * @return string
 */
function asset_path( $file_path ) {
	$dist_path = get_template_directory_uri() . '/dist/';
	return $dist_path . $file_path;
}

/**
 * Get modified time for given asset file
 *
 * @param string $file_path File path.
 * @return int
 */
function asset_mtime( $file_path ) {
	$dist_path = get_template_directory() . '/dist/';
	return file_exists( $dist_path . $file_path ) ? filemtime( $dist_path . $file_path ) : 0;
}
