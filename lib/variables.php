<?php

define( '__ROOT__', get_template_directory_uri() );
define( '__ROOT_PATH__', get_template_directory() );
define( '__ASSETS__', __ROOT__ . '/dist' );
define( '__IMAGES__', __ASSETS__ . '/images' );
define( '__HOME__', get_home_url() );
define( '__SITE_URL__', site_url() );
define( '__AJAX_URL__', admin_url( 'admin-ajax.php' ) );

