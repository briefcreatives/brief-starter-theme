<?php

namespace Brief\Setup;

use Brief\Assets;

/**
 * Theme setup
 */
function setup() {

	// Make theme available for translation
	// Community translations can be found at https://github.com/roots/sage-translations
	load_theme_textdomain( 'brief-theme', get_template_directory() . '/language' );

	// Enable plugins to manage the document title
	// http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
	add_theme_support( 'title-tag' );

	// Register wp_nav_menu() menus
	// http://codex.wordpress.org/Function_Reference/register_nav_menus
	register_nav_menus(
		[ 'primary_navigation' => __( 'Primary Navigation', 'brief-theme' ) ]
	);

	// Enable post thumbnails
	// http://codex.wordpress.org/Post_Thumbnails
	// http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
	// http://codex.wordpress.org/Function_Reference/add_image_size
	add_theme_support( 'post-thumbnails' );

	// Enable post formats
	// http://codex.wordpress.org/Post_Formats
	add_theme_support( 'post-formats', [ 'aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio' ] );

	// Enable HTML5 markup support
	// http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
	add_theme_support( 'html5', [ 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ] );

	// Use main stylesheet for visual editor
	add_editor_style( Assets\asset_path( 'styles/main.css' ) );
}
add_action( 'after_setup_theme', __NAMESPACE__ . '\\setup' );

/**
 * Theme assets
 */
function assets() {
	wp_enqueue_style( 'brief/css', Assets\asset_path( 'styles/main.css' ), false, Assets\asset_mtime( 'styles/main.css' ) );

	wp_deregister_script( 'jquery' );
	wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js', array(), null, false );
	wp_enqueue_script( 'wp-api' );
	wp_enqueue_script( 'brief/js', Assets\asset_path( 'scripts/main.js' ), [ 'jquery' ], Assets\asset_mtime( 'scripts/main.js' ), true );

	wp_localize_script(
		'brief/js',
		'wp_vars',
		[
			'ajax_url' => __AJAX_URL__,
			'home'     => __HOME__,
			'assets'   => __ASSETS__,
		]
	);
}
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100 );
