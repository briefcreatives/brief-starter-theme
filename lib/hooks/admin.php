<?php

/**
 * Hooks - Admin Interface
 */

 // Login Logo
add_action(
	'login_enqueue_scripts',
	function(){ ?>
		<style type="text/css">
			#login h1 a,
			.login h1 a {
			width: 320px;
			height: 125px;
			background-size: 75%;
			background-position: center;
			background-repeat: no-repeat;
			background-image: url(<?php echo __IMAGES__; ?>/admin-login-logo.png);
			}
		</style>
		<?php
	}
);

// Login header url
add_filter(
	'login_headerurl',
	function() {
		return home_url();
	}
);

// Footer text
add_filter(
	'admin_footer_text',
	function() {
		echo '<span id="footer-thankyou">Developed by <a href="https://brief.pt" target="_blank">Brief Creatives</a></span>.';
	}
);


// Admin favicon
add_action( 'login_head', 'admin_favicon' );
add_action( 'admin_head', 'admin_favicon' );
function admin_favicon() {
	echo '<link rel="shortcut icon" href="' . __IMAGES__ . '/favicon/favicon.ico" />';
}



