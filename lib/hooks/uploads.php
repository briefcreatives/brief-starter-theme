<?php

/**
 * Hooks - Uploads
 */

/* ==========================================================================
   CHANGE UPLOAD LIMIT by Brief
   ========================================================================== */

add_filter( 'upload_size_limit', 'brief_increase_upload' );
function brief_increase_upload( $bytes ) {
    return 33554432; // 32 megabytes
}