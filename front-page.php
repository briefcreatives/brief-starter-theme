<?php // Frontpage

// * Timber Context
$context = Timber::get_context();

// * Post
$context['post'] = new \Timber\Post();

// * Load view
Timber::render('views/front-page.twig', $context);
