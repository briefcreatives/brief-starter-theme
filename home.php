<?php

/**
 * Archive Page
 */

// * Timber Context
$context = Timber::get_context();

// * Posts
$context['posts'] = new \Timber\PostQuery();

// * Load view
Timber::render( 'views/archive.twig', $context );
