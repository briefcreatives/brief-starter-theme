<?php

use Brief\Setup;
use Brief\Wrapper; ?>

<!doctype html>
<html <?php language_attributes(); ?>>
	<?php get_template_part( 'templates/head' ); ?>
	<body <?php body_class(); ?>>
		<?php get_template_part( 'templates/navbar' ); ?>
		<div class="uk-container uk-container-center" role="document">
			<div class="content">
				<main class="main">
					<?php require Wrapper\template_path(); ?>
				</main><!-- /.main -->
			</div><!-- /.content -->
		</div><!-- /.uk-container -->
		<?php
		get_template_part( 'templates/footer' );
		wp_footer();
		?>
	</body>
</html>
